package ua.oop.khpi.lymar13;

import ua.oop.khpi.lymar09.ContainerList;

public class FirstThread extends Thread {
    private int[] listToArr;
    /**
     * Timer value.
     * Thread is working,while a value > 0.
     */
    private double timeOut;
    /**
     * Constructor.
     * Set a values and timer.
     * @param list data for working
     * @param timeOut set timer
     */
    public FirstThread(ContainerList<Integer> list, double timeOut) {
        this.timeOut = timeOut;
        Object[] temp = list.toArray();
        this.listToArr = new int[temp.length];
        for(int i = 0; i < temp.length; i++) {
            this.listToArr[i] = (int) temp[i];
        }
    }
    /**
     * Override of run thread method
     * Finding count of even and odd values in an array of numbers.
     */
    @Override
    public void run() {
        long startTime = System.nanoTime();
        int even = 0, odd = 0;
        for (int i = 0; i < listToArr.length; i++) {
            if (listToArr[i] % 2 == 0) {
                even++;
            } else {
                odd++;
            }
            long timeTotal = System.nanoTime() - startTime;
            try {
                double convert = timeTotal*10e-9;
                if(convert > timeOut) {
                    throw new Exception();
                }
            } catch (Exception e) {
                this.interrupt();
                System.out.println("!WARNING!FirstThread was interrupted!");
                return;
            }
        }
        System.out.println("Even: " + even + " | Odd: " + odd);
    }
}
