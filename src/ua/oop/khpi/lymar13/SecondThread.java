package ua.oop.khpi.lymar13;

import ua.oop.khpi.lymar09.ContainerList;

public class SecondThread extends Thread {
    private int[] listToArr;
    /**
     * Timer value.
     * Thread is working,while a value > 0.
     */
    private double timeOut;
    /**
     * Constructor.
     * Set a values and timer.
     * @param list data for working
     * @param timeOut set timer
     */
    public SecondThread(ContainerList<Integer> list, double timeOut) {
        this.timeOut = timeOut;
        Object[] temp = list.toArray();
        this.listToArr = new int[temp.length];
        for(int i = 0; i < temp.length; i++) {
            this.listToArr[i] = (int) temp[i];
        }
    }
    /**
     * Override of run thread method
     * Finding an AVG of integer array value.
     */
    @Override
    public void run() {
        long startTime = System.nanoTime();
        float avg = 0;
        for (int i = 0; i < listToArr.length; i++) {
            avg += listToArr[i];
            long timeTotal = System.nanoTime() - startTime;
            try {
                double convert = timeTotal*10e-9;
                if(convert > timeOut) {
                    throw new Exception();
                }
            } catch (Exception e) {
                this.interrupt();
                System.out.println("!WARNING!SecondThread was interrupted!");
                return;
            }
        }
        avg /= listToArr.length;
        System.out.println("AVG: " + avg);
    }
}
