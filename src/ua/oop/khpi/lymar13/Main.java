package ua.oop.khpi.lymar13;

import ua.oop.khpi.lymar09.ContainerList;
import java.util.Random;

public class Main {
    /**
     * An entry point - main method.
     *
     * @param args - arguments of main method
     */
    public static void main(String[] args) {
        /**
         * List of integer numbers
         */
        ContainerList<Integer> numbers = new ContainerList<>();
        /**
         * Filling a list by random numbers
         */
        for (int i = 0; i < 100000; i++) {
            numbers.pushBack(new Random().nextInt(100000));
        }
        /**
         * Making three threads and setting time out on 0.5 sec
         */
        Thread thr1 = new FirstThread(numbers, 0.2);
        Thread thr2 = new SecondThread(numbers, 0.5);
        Thread thr3 = new ThirdThread(numbers, 0.5);
        /**
         * Use start method of thread class for run our threads
         */
        thr1.start();
        thr2.start();
        thr3.start();

    }
}
