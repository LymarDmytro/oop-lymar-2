package ua.oop.khpi.lymar13;


import ua.oop.khpi.lymar09.ContainerList;

public class ThirdThread extends Thread {
    private int[] listToArr;
    /**
     * Timer value.
     * Thread is working,while a value > 0.
     */
    private double timeOut;
    /**
     * Constructor.
     * Set a values and timer.
     * @param list data for working
     * @param timeOut set timer
     */
    public ThirdThread(ContainerList<Integer> list, double timeOut) {
        this.timeOut = timeOut;
        Object[] temp = list.toArray();
        this.listToArr = new int[temp.length];
        for(int i = 0; i < temp.length; i++) {
            this.listToArr[i] = (int) temp[i];
        }
    }
    /**
     * Override of run thread method
     * Find a min and max value of integer digits.
     */
    @Override
    public void run() {
        long startTime = System.nanoTime();
        int min = Integer.MAX_VALUE;
        int max = 0;
        for (int i = 0; i < listToArr.length; i++) {
            if (min > listToArr[i]) {
                min = listToArr[i];
            } else if (max < listToArr[i]) {
                max = listToArr[i];
            }
            long timeTotal = System.nanoTime() - startTime;
            try {
                double convert = timeTotal*10e-9;
                if(convert > timeOut) {
                    throw new Exception();
                }
            } catch (Exception e) {
                this.interrupt();
                System.out.println("!WARNING!ThirdThread was interrupted!");
                return;
            }
        }
        System.out.println("Min: " + min + " | Max: " + max);
    }
}
