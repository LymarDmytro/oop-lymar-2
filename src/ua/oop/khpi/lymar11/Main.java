package ua.oop.khpi.lymar11;

import ua.oop.khpi.lymar10.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<String> list = Arrays.asList(args);
        boolean autoMode = list.contains("-auto");
        boolean ContBreak = false;
        while(!ContBreak) ContBreak = UI.run(autoMode);
    }
}