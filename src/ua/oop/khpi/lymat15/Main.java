package ua.oop.khpi.lymar15;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        boolean isExit = false;
        List<String> paramList;
        paramList = Arrays.asList(args);
        boolean isAuto = paramList.contains("-auto");
        if (isAuto) {

        } else {
            while (!isExit) {
                isExit = UI.run();
            }
        }
    }
}
