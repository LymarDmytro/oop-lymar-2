package ua.oop.khpi.lymar15;

import ua.oop.khpi.lymar09.ContainerList;
import ua.oop.khpi.lymar10.SortByDepartureTime;
import ua.oop.khpi.lymar10.SortByFlightNumber;
import ua.oop.khpi.lymar10.SortByNumberOfFreeSeats;
import ua.oop.khpi.lymar07.BusStation;
import ua.oop.khpi.lymar12.StationSearch;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class UI {
    /**
     * Linked list of entries of Bus Station
     */
    private static LinkedList<BusStation> entries = new LinkedList<>();
    private static String choice;
    private static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    /**
     * The main method thats runs main menu
     *
     * @return true if user choose exit
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static boolean run() throws IOException, ClassNotFoundException {
        mainMenu();
        boolean flag = mainProcessing();
        System.out.println();
        return flag;
    }

    private static void mainMenu() throws IOException {
        System.out.println("\n1. Добавить запись в расписание.");
        System.out.println("2. Удалить запись.");
        System.out.println("3. Очистить список записей.");
        System.out.println("4. Вывод информации.");
        System.out.println("5. Сортировка.");
        System.out.println("6. Serialization ");
        System.out.println("7. Deserialization");
        System.out.println("8. Сохранить в файл");
        System.out.println("9. Прочитать с файла");
        System.out.println("10. Поиск записей по номеру рейса");
        System.out.println("0. Выход.");
        System.out.print("Введите ваш ответ сюда: ");
        choice = buffer.readLine();
        System.out.println();
    }
    /**
     * Обработка выбора главного меню.
     * @return true - если выход
     * @throws IOException при ошибках со вводом
     * @throws ClassNotFoundException при ошибке с классами
     */
    private static boolean mainProcessing() throws IOException, ClassNotFoundException {
        switch (choice) {
            case "1":
                onAdd();
                addProcessing();
                return false;

            case "2":
                onDelete();
                deleteProcessing();
                return false;

            case "3":
                if(entries.size()== 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.println("Очистка...");
                entries.clear();
                return false;

            case "4":
                if(entries.size() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.println("Данные: ");
                System.out.print(entries.toString());
                return false;

            case "5":
                if(entries.size() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                onSort();
                sortProcessing();
                return false;

            case "6":
                serialize();
                return false;

            case "7":
                deserialize();
                return false;
            case "8":
                saveToFile();
                return false;
            case "9":
                loadFromFile();
                return false;
            case "10":
                if(entries.size() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.println("Введите номер рейса по которому вы хотите найти: ");
                String number = buffer.readLine();
                BusStation[] anotherEntries = new BusStation[entries.size()];
                for (int i = 0, j = 0; i < entries.size(); i++) {
                    if(entries.get(i).getFlightNumberStringVersion().equals(number)) {
                        anotherEntries[j++] = entries.get(i);
                    }
                }
                for (BusStation entry : anotherEntries) {
                    if (entry != null) {
                        System.out.println(entry.toString());
                    }
                }
                return false;
            case "0":
                System.out.print("Спасибо за работу!!!");
                return true;
            default:
                return false;
        }
    }
    /**
     * Меню добавления записи.
     * @throws IOException при ошибках со вводом
     */
    private static void onAdd() throws IOException {
        System.out.println("1. Добавить в начало списка.");
        System.out.println("2. Прочитать из файла");
        System.out.println("Любая клавиша. Назад.");
        System.out.print("Введите ваш ответ сюда: ");
        choice = buffer.readLine();
        System.out.println();
    }

    private static void addProcessing() throws IOException {
        switch (choice) {
            case "1":
                entries.add(BusStation.generateEntry());
                break;

            case "2":
                LinkedList<BusStation> temp = new LinkedList<>(Arrays.asList(BusStation.readFromFile("data.txt")));
                for (BusStation entry : temp) {
                    if(entry != null) {
                        entries.add(entry);
                    }
                }
                break;
        }
    }
    /**
     * Меню удаления записи.
     * @throws IOException при ошибках со вводом
     */
    private static void onDelete() throws IOException {
        if (entries.size() == 0) {
            System.out.println("Список пуст!");
        } else {
            System.out.println("1. Удалить последний.");
            System.out.println("2. Удалить по индексу.");
            System.out.println("Любая клавиша. Назад.");
            System.out.print("Введите ваш ответ сюда: ");
            choice = buffer.readLine();
            System.out.println();
        }
    }
    
    /**
     * Обработка выбора меню удаления.
     * @throws IOException при ошибках со вводом
     */
    private static void deleteProcessing() throws IOException {
        Scanner scan = new Scanner(System.in);
        switch (choice) {

            case "1" :
                entries.removeLast();
                break;

            case "2" :
                System.out.print("Введите индекс:");
                entries.remove(scan.nextInt());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + choice);
        }
    }
    /**
     * Меню сортировки рейсов.
     * @throws IOException при ошибках со вводом
     */
    private static void onSort() throws IOException {
        if (entries.size() == 0) {
            System.out.println("Список пуст!");
        } else {
            System.out.println("1. Сортировка по номеру рейса.");
            System.out.println("2. Сортировка по времени отправления.");
            System.out.println("3. Сортировка по количеству свободных мест.");
            System.out.println("Любая клавиша. Назад.");
            System.out.print("Введите ваш ответ сюда: ");
            choice = buffer.readLine();
            System.out.println();
        }
    }
    /** Обработка выбора меню сортировки. */
    private static void sortProcessing() {
        switch (choice) {
            case "1":
                entries.sort(new SortByFlightNumber(null));
                break;

            case "2":
                entries.sort(new SortByDepartureTime(null));
                break;

            case "3":
                entries.sort(new SortByNumberOfFreeSeats(null));
                break;
        }
    }
    /** Сохранение данных стандартной сериализацией. */
    private static void serialize() {
        System.out.println("Serialization...");
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("DataFile.dat"));
            oos.writeObject(entries);
        } catch(IOException e) {
            System.out.println(e.toString());
        }
        System.out.println("Done!");
    }
    /** Чтение данных стандартной сериализацией. */
    private static void deserialize() throws ClassNotFoundException {
        System.out.println("Deserialization...");
        try {
            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("DataFile.dat"));
            LinkedList list_copy =
                    (LinkedList) ois.readObject();
            System.out.println("Data after reading: ");
            System.out.println(list_copy.toString());
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
    /** Сохранение данных с помощью XML. */
    private static void saveToFile(){
        System.out.println("Сохраненеие в XML...");
        try {
            FileOutputStream fos = new FileOutputStream("Encoded.xml");
            XMLEncoder xmlEncoder = new XMLEncoder(new BufferedOutputStream(fos));
            xmlEncoder.writeObject(entries);
            xmlEncoder.close();
        } catch(FileNotFoundException e) {
            System.out.println(e.toString());
        }
        System.out.println("Done!");
    }
    /** Чтение данных с помощью XML. */
    private static void loadFromFile() {
        System.out.println("Чтение из XML...");
        try {
            FileInputStream fis = new FileInputStream("Encoded.xml");
            XMLDecoder xmlDecoder = new XMLDecoder(
                    new BufferedInputStream(fis));
            LinkedList list2 =
                    (LinkedList) xmlDecoder.readObject();
            System.out.print(list2.toString());
            xmlDecoder.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
