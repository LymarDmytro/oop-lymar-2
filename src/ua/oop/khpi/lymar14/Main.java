package ua.oop.khpi.lymar14;

import ua.oop.khpi.lymar09.ContainerList;
import ua.oop.khpi.lymar13.FirstThread;
import ua.oop.khpi.lymar13.SecondThread;
import ua.oop.khpi.lymar13.ThirdThread;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Random;

import static javax.swing.JLabel.*;

public class Main {
    private static final int DIVIDER = 1_000_000;
    /**
     * A method for creating a table.
     * @param sequentialTime time of sequential processing
     * @param synchronousTime time of synchronous processing
     * @param DifferenceOfTime difference of time
     */
    public static void createTable(int size,double sequentialTime, double synchronousTime, double DifferenceOfTime) {
        String[] columnNames = {"Size of Array", "Synchronous Processing", "Sequential Processing",
                "Difference"};
        String[][] data = {{Integer.toString(size), Double.toString(sequentialTime),
                Double.toString(synchronousTime), Double.toString(DifferenceOfTime)}};

        DefaultTableCellRenderer centerRenderer;
        centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( CENTER );
        JTable table = new JTable(data, columnNames);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        table.getColumnModel().getColumn(0).setPreferredWidth(185);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setPreferredWidth(185);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(2).setPreferredWidth(185);
        table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(3).setPreferredWidth(185);
        table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        JScrollPane scrollPane = new JScrollPane(table);
        JFrame frame = new JFrame("Result Of Lab №14");

        frame.getContentPane().add(scrollPane);
        frame.setPreferredSize(new Dimension(760, 300));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) throws InterruptedException {
        ContainerList<Integer> numbers = new ContainerList<>();
        int size = 3000000;
        for (int i = 0; i < size; i++) {
            numbers.pushBack(new Random().nextInt(20000));
        }
        Thread thr1 = new FirstThread(numbers, 10000);
        Thread thr2 = new SecondThread(numbers, 10000);
        Thread thr3 = new ThirdThread(numbers, 10000);

        double corrTime = System.nanoTime();
        thr1.start();
        thr2.start();
        thr3.start();

        thr1.join();
        thr2.join();
        thr3.join();
        corrTime = (System.nanoTime() - corrTime) * 10e-10;
        System.out.print("\n");
        double sequentialTime = System.nanoTime();
        thr1.run();
        thr2.run();
        thr3.run();
        sequentialTime = (System.nanoTime() - sequentialTime) * 10e-10;
        double timeToDiff = sequentialTime / corrTime;
        Main.createTable(size, corrTime, sequentialTime, timeToDiff);
    }
}
