package ua.oop.khpi.lymar07;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Flight  implements Serializable {

    private String nameOfStation;
    private String arrivalTime;

    public Flight() {
        this.nameOfStation = null;
        this.arrivalTime = null;
    }
    public Flight(String nofs, String at) {
        this.setNameOfStation(nofs);
        this.setArrivalTime(at);
    }

    public String getNameOfStation() {
        return nameOfStation;
    }

    public void setNameOfStation(String nameOfStation) {
        if (this.checkNameOfStation(nameOfStation))
        this.nameOfStation = nameOfStation;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        if (this.checkArrivalTime(arrivalTime))
        this.arrivalTime = arrivalTime;
    }

    public boolean checkNameOfStation (String nameOfStation) {
        Pattern pattern = Pattern.compile("^([A-Z])[a-z]+");
        Matcher matcher = pattern.matcher(nameOfStation);
        return matcher.matches();
    }

    public boolean checkArrivalTime (String arrivalTime) {
        Pattern pattern = Pattern.compile("(2[0-3]|[0-1]\\d):[0-5]\\d");
        Matcher matcher = pattern.matcher(arrivalTime);
        boolean s  = matcher.matches();
        return matcher.matches();
    }

    @Override
    public String toString() {
        return nameOfStation + " " + arrivalTime;
    }
}
