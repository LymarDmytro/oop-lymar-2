package ua.oop.khpi.lymar07;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Bus Station class.
 * Class defines the entity of a special task.
 * Schedule entries that contains of flightNumber,departureTime etc.
 */
public class BusStation implements Serializable {
    private int flightNumber;       // A flight number in Schedule
    private String departureTime;   // departure time of the bus
    private String dayOfTheWeek;    // day of the week when the bus travels
    private int numberOfFreeSeats;  // count of free seats in bus
    private List<Flight> route;     // The bus route (name of station, arrival time)

    /**
     Default constructor
     */
    public BusStation() {
        flightNumber = 0;
        departureTime = null;
        dayOfTheWeek = null;
        numberOfFreeSeats = 0;
        route = new ArrayList<>();
    }

    /**
     * The setters of our information variables
     */
    public void setFlightNumber(int flightNumber) {
        if(this.checkFlightNumber(String.valueOf(flightNumber)))
        this.flightNumber = flightNumber;
    }
    public void setDepartureTime(String departureTime) {
        if(this.checkDepartureTime(departureTime))
        this.departureTime = departureTime;
    }
    public void setDayOfTheWeek(String dayOfTheWeek) {
        if (this.checkDayOfWeek(dayOfTheWeek))
        this.dayOfTheWeek = dayOfTheWeek;
    }
    public void setNumberOfFreeSeats(int numberOfFreeSeats) {
        if (this.checkNumberOfFreeSeats(String.valueOf(numberOfFreeSeats)))
        this.numberOfFreeSeats = numberOfFreeSeats;
    }
    public void setRoute(final List<Flight> route) {
        if (this.route.size() == 0) {
            this.route.addAll(route);
        }
    }

    /**
     * The getters of our information variables
     */
    public int getFlightNumber() {
        return this.flightNumber;
    }

    public String getFlightNumberStringVersion(){
        return Integer.toString(this.flightNumber);
    }
    public int getNumberOfFreeSeats() {
        return numberOfFreeSeats;
    }
    public String getDepartureTime() {
        return departureTime;
    }
    public String getDayOfTheWeek() {
        return dayOfTheWeek;
    }
    public List<Flight> getRoute (){
        return route;
    }


    /**
     * Adding stations in the route.
     * @param stationNum - the number of stations
     * @throws IOException - if there is any unresolved input/output
     */
    public void enterRoute(int stationNum) throws IOException {

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Введите " + stationNum + " станций");
        String station;
        String time;
        for (int i = 0; i < stationNum; i++) {
            System.out.print((i + 1) + ".\n");
            System.out.print("Введите название: ");
            station = reader.readLine();
            System.out.print("Введите время прибытия: ");
            time = reader.readLine();
            this.route.add(new Flight(station, time));
        }
    }

    public static BusStation generateEntry() throws IOException{
        Scanner in = new Scanner(System.in);
        Scanner in2 = new Scanner(System.in);
        BusStation entry = new BusStation();
        System.out.print("Введите номер рейса: ");
        entry.setFlightNumber(in.nextInt());
        System.out.print("Введите время отправления: ");
        entry.setDepartureTime(in2.nextLine());
        System.out.print("Введите день недели: ");
        entry.setDayOfTheWeek(in2.nextLine());
        System.out.print("Введите кол-во свободных мест: ");
        entry.setNumberOfFreeSeats(in.nextInt());
        System.out.print("Введите количество станций: ");
        int countOfStations = in.nextInt();
        in.nextLine();
        entry.enterRoute(countOfStations);
        return entry;
    }

    /**
     * Создание записи в журнале по умолчанию.
     * @param isNull - определяет как создать объект
     * @return новая запись
     */
    public static BusStation generateEntry(final boolean isNull) {
        BusStation entry = new BusStation();
        if (isNull) {
            entry.setFlightNumber(0);
            entry.setDepartureTime(null);
            entry.setDayOfTheWeek(null);
            entry.setNumberOfFreeSeats(0);
            return entry;
        } else {
            entry.setFlightNumber(444);
            entry.setDepartureTime("12:00");
            entry.setDayOfTheWeek("Friday");
            entry.setNumberOfFreeSeats(10);
            entry.setRoute(
                    new ArrayList<>(Arrays.asList(new Flight("Odessa","12:00"))));
            return entry;
        }
    }

    @Override
    public String toString() {
        return  "\nНомер рейса: " + flightNumber + "\n" +
                "Время отправления: " + departureTime + '\n' +
                "День недели: " + dayOfTheWeek + '\n' +
                "Кол-во свободных мест: " + numberOfFreeSeats + '\n' +
                "Маршрут: " + route.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return (this.toString().equals(obj.toString()));
    }

    public static BusStation[] readFromFile(final String filename) throws IOException {
        FileReader fr = new FileReader(filename);
        Scanner scan = new Scanner(fr);
        List<Flight> lst = new ArrayList<>();
        BusStation[] se = new BusStation[16];
        int i = 0;
        int temp = 0;
        String station;
        String time;
        while(scan.hasNextLine()) {
            se[i] = new BusStation();
            se[i].setFlightNumber(Integer.parseInt(scan.nextLine()));
            se[i].setDepartureTime(scan.nextLine());
            se[i].setDayOfTheWeek(scan.nextLine());
            se[i].setNumberOfFreeSeats(Integer.parseInt(scan.nextLine()));
            temp = Integer.parseInt(scan.nextLine());
            while(temp > 0) {
                station = scan.nextLine();
                time = scan.nextLine();
                lst.add(new Flight(station ,time));
                temp--;
            }
            se[i++].setRoute(lst);
            lst.clear();
        }
        scan.close();
        fr.close();
        return se;
    }

    public boolean checkFlightNumber (String flightNumber) {
        Pattern pattern = Pattern.compile("^\\d{3}");
        Matcher matcher = pattern.matcher(flightNumber);
        return matcher.matches();
    }
    public boolean checkDepartureTime(String departureTime) {
        Pattern pattern = Pattern.compile("(2[0-3]|[0-1]\\d):[0-5]\\d");
        Matcher matcher = pattern.matcher(departureTime);
        boolean ss  = matcher.matches();
        return matcher.matches();
    }
    public boolean checkDayOfWeek (String dayOfTheWeek) {
        Pattern pattern = Pattern.compile("(?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)");
        Matcher matcher = pattern.matcher(dayOfTheWeek);
        return matcher.matches();
    }
    public boolean checkNumberOfFreeSeats (String numberOfFreeSeats) {
        Pattern pattern = Pattern.compile("^(5[0]|[0-4]\\d|[0-9])");
        Matcher matcher = pattern.matcher(numberOfFreeSeats);
        return matcher.matches();
    }


}

