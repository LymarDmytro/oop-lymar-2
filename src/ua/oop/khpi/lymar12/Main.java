package ua.oop.khpi.lymar12;

        import ua.oop.khpi.lymar07.BusStation;
        import ua.oop.khpi.lymar07.Flight;
        import ua.oop.khpi.lymar09.ContainerList;
        import ua.oop.khpi.lymar10.UI;

        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.List;

public class Main  {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<String> list = Arrays.asList(args);
        boolean isExit = false;
        boolean isAuto = list.contains("-auto");
        if (isAuto) {
            ContainerList<BusStation> flights = new ContainerList<>();
            flights.pushFront(BusStation.generateEntry(false));
            BusStation autoFlight = new BusStation();
            autoFlight.setFlightNumber(123);
            autoFlight.setDepartureTime("08:30");
            autoFlight.setDayOfTheWeek("Sunday");
            autoFlight.setRoute(
                    new ArrayList<>(Arrays.asList(new Flight("Kharkiv","12:00"),
                            new Flight("Poltava","10:00"),
                            new Flight("Kyiv","12:00"))));
            flights.pushBack(autoFlight);
            System.out.print("Список:");
            System.out.println(flights.toString());
            System.out.println("Поиск по нужному условию:");
            System.out.print(StationSearch.searchEntries(flights));
        } else {
            while (!isExit) {
                isExit = UI.run(false);
            }
        }
    }
}
