package ua.oop.khpi.lymar12;

import ua.oop.khpi.lymar07.Flight;
import ua.oop.khpi.lymar09.ContainerList;
import ua.oop.khpi.lymar07.BusStation;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StationSearch {

    public static String searchEntries (final ContainerList<BusStation> list){
        BusStation[] entries = dayOff(list);
        StringBuilder found = new StringBuilder();
        for (BusStation entry : entries){
            if (entry != null){
                found.append(entry.toString()).append("\n");
            }
        }
        if (found.length() == 0){
            return "Not found";
        }
        return found.toString();
    }

    private static BusStation[] dayOff(final ContainerList<BusStation> list) {
        BusStation[] neededRout = morningFlightTime(list);
        BusStation[] entries = new BusStation[neededRout.length];
        for (int i = 0, j = 0; i < neededRout.length; i++) {
            if (neededRout[i] != null) {
                entries[j++] = neededRout[i];
            }
        }
        Pattern pattern;
        Matcher matcher;
        final String REGEX_DAY = "(?:Saturday|Sunday)";
        pattern = Pattern.compile(REGEX_DAY);
        for (int i = 0; i < entries.length; i++) {
            if (entries[i] != null) {
                matcher = pattern.matcher(entries[i].getDayOfTheWeek());
                if (!matcher.matches()) {
                    entries[i] = null;
                }
            }
        }
        return entries;
    }

    private static BusStation[] morningFlightTime(final ContainerList<BusStation> list) {
        BusStation[] neededRout = neededRout(list);
        BusStation[] entries = new BusStation[neededRout.length];
        for (int i = 0, j = 0; i < neededRout.length; i++) {
            if (neededRout[i] != null) {
                entries[j++] = neededRout[i];
            }
        }
        Pattern pattern;
        Matcher matcher;
        final String REGEX = "(0[6-9]|1[0-1]):[0-5]\\d";
        pattern = Pattern.compile(REGEX);
        for (int i = 0; i < entries.length; i++) {
            if (entries[i] != null) {
                matcher = pattern.matcher(entries[i].getDepartureTime());
                if (!matcher.matches()) {
                    entries[i] = null;
                }
            }
        }
        return entries;
    }

    private static BusStation[] neededRout(final ContainerList<BusStation> list) {
        Object[] arr = list.toArray();
        BusStation[] entry = new BusStation[arr.length];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                entry[j++] = (BusStation) arr[i];
            }
        }
        Pattern pattern;
        Matcher matcher;
        final String REGEX_BOUNDS = "(?:Kharkiv|Kyiv|Poltava)";
        pattern = Pattern.compile(REGEX_BOUNDS);
        List<Flight> routs;
        for (int i = 0; i < entry.length; i++) {
            routs = entry[i].getRoute();
            for(int j = 0; j < routs.size(); j++){
                Flight temp = routs.get(j);
                matcher = pattern.matcher(temp.getNameOfStation());
                if (!matcher.matches()) {
                    entry[i] = null;
                }
            }
        }
        return entry;
    }
}
