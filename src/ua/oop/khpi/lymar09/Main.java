package ua.oop.khpi.lymar09;

import ua.oop.khpi.lymar07.BusStation;


import java.io.IOException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Arrays;

/**
 * Entry class.
 * Contains entry point of a program.
 *
 * @author lymar-dmytro
 */
public class Main {

/**
 * Main method.
 * List manipulation is demonstrated in this code snippet.
 *
 * @param args - command line parameters
 * @throws IOException - if there are collisions in input/output
 * @throws ClassNotFoundException - if class cannot be resolved
 */
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        ContainerList<BusStation> firstContainer = new ContainerList<>();

        System.out.println("Добавление узла в начало списка...");
        firstContainer.pushFront(BusStation.generateEntry());
        System.out.println("\nСписок: ");
        System.out.println(firstContainer.toString());

        System.out.println("\nДобавление узла в конец списка...");
        firstContainer.pushBack(BusStation.generateEntry());
        System.out.println("\nСписок: ");
        System.out.println(firstContainer.toString());

        System.out.println("Удаление последнего элемента списка...");
        firstContainer.popBack();
        System.out.println("\nСписок: ");
        System.out.println(firstContainer.toString());

        System.out.println("Превращение списка в массив:");
        System.out.println(Arrays.toString(firstContainer.toArray()));

        System.out.println("\nCериализация...");
        ObjectOutputStream ser = new ObjectOutputStream(
                new FileOutputStream("Vadim.dat"));
        ser.writeObject(firstContainer);
        ser.close();
        System.out.println("Успешно!\n");
        System.out.println("Десериализация...");
        ObjectInputStream inSer = new ObjectInputStream(
                new FileInputStream("Vadim.dat"));
        ContainerList<BusStation> secondContainer =
                (ContainerList) inSer.readObject();
        inSer.close();
        System.out.print(secondContainer.toString());

        System.out.println("\nСохраненеие в XML...");
        FileOutputStream fos = new FileOutputStream("Encoded.xml");
        XMLEncoder xmlEncoder = new XMLEncoder(new BufferedOutputStream(fos));
        xmlEncoder.writeObject(firstContainer);
        xmlEncoder.close();
        System.out.println("Сохранено!\n");

        System.out.println("Чтение из XML...");
        try {
            FileInputStream fis = new FileInputStream("Encoded.xml");
            XMLDecoder xmlDecoder = new XMLDecoder(
                    new BufferedInputStream(fis));
            ContainerList<BusStation> thirdConteiner =
                    (ContainerList) xmlDecoder.readObject();
            xmlDecoder.close();
            System.out.print(thirdConteiner.toString());
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        System.out.println("\n\nПроверка на наличие элемента в узле...");
        if(firstContainer.contains(BusStation.generateEntry()))
            System.out.println("Содержит");
        else
            System.out.println("Не содержит");

        System.out.println("\nОчистка списка...");
        firstContainer.clear();
        System.out.println("Успешно!");
        System.out.println("\nСписок: ");
        System.out.println(firstContainer.toString());
    }
}




