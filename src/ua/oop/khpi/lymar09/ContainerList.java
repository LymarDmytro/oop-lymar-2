package ua.oop.khpi.lymar09;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class ContainerList<T> implements Iterable<T>, Serializable {
    /**
     * Serialization ID
     */
    private static final long serialVersionUID = 711121452459854465L;

    /**
     * The main body for the every node in List
     */
    public static class Node<T> implements Serializable {
        /**
         * Serialization ID
         */
        private static final long serialVersionUID = 419332152290841547L;
        private Node<T> prev;   // A pointer on prev list node
        private Node<T> next;   // A pointer on next list node
        private T data;         // A data of the node

        /**
         * Default constructor
         */
        public Node() {
            this.prev = null;
            this.next = null;
            this.data = null;
        }

        /**
         * Constructor for the creating list node
         */
        Node(final Node<T> previosly, final T data, final Node<T> next) {
            this.prev = previosly;
            this.next = next;
            this.data = data;
        }

        public T getData() {
            return this.data;
        }

        /**
         * Getter for next field.
         *
         * @return next
         */
        public Node<T> getNext() {
            return this.next;
        }

        /**
         * Getter of prev field.
         *
         * @return prev
         */
        public Node<T> getPrev() {
            return this.prev;
        }

        /**
         * Setter for data field.
         *
         * @param d - data
         */
        public void setData(final T d) {
            this.data = d;
        }

        /**
         * Setter for next field.
         *
         * @param n - next
         */
        public void setNext(final Node<T> n) {
            this.next = n;
        }

        /**
         * Setter for prev field.
         *
         * @param p - prev
         */
        public void setPrev(final Node<T> p) {
            this.prev = p;
        }
    }

    /**
     * Pointer on first node of list.
     */
    private Node<T> first;
    /**
     * Pointer on last node of list.
     */
    private Node<T> last;
    /**
     * Count of nodes in list.
     */
    private int size;

    /**
     * Getter for size field.
     *
     * @return размер списка
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Getter for first field.
     *
     * @return first
     */
    public Node<T> getFirst() {
        return first;
    }

    /**
     * Getter for last field.
     *
     * @return last
     */
    public Node<T> getLast() {
        return last;
    }

    /**
     * Setter for size field.
     *
     * @param s - size
     */
    public void setSize(final int s) {
        this.size = s;
    }

    /**
     * Setter for size field.
     *
     * @param f - first
     */
    public void setFirst(final Node<T> f) {
        this.first = f;
    }

    /**
     * Setter for the last field.
     *
     * @param l - last
     */
    public void setLast(final Node<T> l) {
        this.last = l;
    }

    /**
     * Constructor for creation of the List.
     */
    public ContainerList() {
        this.size = 0;
    }

    /**
     * Retrun data of first node.
     *
     * @return data of first node
     */
    public T first() {
        Node<T> f = this.first;
        if (f == null) {
            throw new NoSuchElementException();
        } else {
            return f.data;
        }
    }

    /**
     * Retrun data of last node.
     *
     * @return data of last node
     */
    public T last() {
        Node<T> l = this.last;
        if (l == null) {
            throw new NoSuchElementException();
        } else {
            return l.data;
        }
    }

    /**
     * Push data on front of the list.
     *
     * @param data - our data
     */
    public void pushFront(final T data) {
        Node<T> f = this.first;
        Node<T> newNode = new Node<T>(null, data, f);
        this.first = newNode;
        if (f == null) {
            this.last = newNode;
        } else {
            f.prev = newNode;
        }
        this.size++;
    }

    /**
     * Push data in the back of the list.
     *
     * @param data - данные
     */
    public void pushBack(final T data) {
        Node<T> l = this.last;
        Node<T> newNode = new Node<T>(l, data, null);
        this.last = newNode;
        if (l == null) {
            this.first = newNode;
        } else {
            l.next = newNode;
        }
        this.size++;
    }

    /**
     * Push node after nedded node
     * (if exist).
     *
     * @param data - data
     * @param curr - node
     */
    private void pushBefore(final T data, final Node<T> curr) {
        Node<T> pred = curr.prev;
        Node<T> newNode = new Node<T>(pred, data, curr);
        curr.prev = newNode;
        if (pred == null) {
            this.first = newNode;
        } else {
            pred.next = newNode;
        }
        this.size++;
    }

    /**
     * Insert a node by needed index.
     *
     * @param index - index
     * @param data  - data
     */
    public void insert(final int index, final T data) {
        this.checkPositionIndex(index);
        if (index == this.size) {
            this.pushBack(data);
        } else {
            this.pushBefore(data, this.getNodeByIndex(index));
        }
    }

    /**
     * Delete last node of the list.
     */
    public void popBack() {
        Node<T> l = this.last;
        if (l == null) {
            throw new NoSuchElementException();
        } else {
            T element = l.data;
            Node<T> prev = l.prev;
            l.data = null;
            l.prev = null;
            this.last = prev;
            if (prev == null) {
                this.first = null;
            } else {
                prev.next = null;
            }
            this.size--;
        }
    }

    /**
     * REMOVE node on needed index.
     *
     * @param index - index
     */
    public void remove(final int index) {
        this.checkElementIndex(index);
        this.unlink(this.getNodeByIndex(index));
    }

    private boolean isElementIndex(final int index) {
        return index >= 0 && index < this.size;
    }

    private boolean isPositionIndex(final int index) {
        return index >= 0 && index <= this.size;
    }

    private String outOfBoundsMsg(final int index) {
        return "Index: " + index + ", Size: " + this.size;
    }

    private void checkElementIndex(final int index) {
        if (!this.isElementIndex(index)) {
            throw new IndexOutOfBoundsException(this.outOfBoundsMsg(index));
        }
    }

    /**
     * Check position index on wrong limit.
     *
     * @param index - индекс
     */
    private void checkPositionIndex(final int index) {
        if (!this.isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(this.outOfBoundsMsg(index));
        }
    }

    /**
     * Unlinking a node of the list.
     *
     * @param x - node
     */
    private void unlink(final Node<T> x) {
        Node<T> next = x.next;
        Node<T> prev = x.prev;
        if (prev == null) {
            this.first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            this.last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.data = null;
        this.size--;
    }

    /**
     * Getting node by index.
     *
     * @param index - index
     * @return node
     */
    private Node<T> getNodeByIndex(final int index) {
        Node<T> x;
        int i;
        if (index < this.size >> 1) {
            x = this.first;
            for (i = 0; i < index; ++i) {
                x = x.next;
            }
        } else {
            x = this.last;
            for (i = this.size - 1; i > index; --i) {
                x = x.prev;
            }
        }
        return x;
    }

    /**
     * Getting data of the node by index.
     *
     * @param index - индекс
     * @return данные узла
     */
    public T getByIndex(final int index) {
        this.checkElementIndex(index);
        return this.getNodeByIndex(index).data;
    }

    /**
     * Getting index of the node.
     *
     * @param o - node
     * @return - index
     */
    public int indexOf(final Object o) {
        int index = 0;
        Node<T> x;
        if (o == null) {
            for (x = this.first; x != null; x = x.next) {
                if (x.data == null) {
                    return index;
                }
                ++index;
            }
        } else {
            for (x = this.first; x != null; x = x.next) {
                if (o.equals(x.data)) {
                    return index;
                }
                ++index;
            }
        }
        return -1;
    }

    /**
     * Checking for contains node in the list.
     *
     * @param o - object
     * @return true, if node exists in the link
     */
    public boolean contains(final Object o) {
        return indexOf(o) >= 0;
    }

    /**
     * Очистка списка.
     */
    public void clear() {
        Node<T> next;
        for (Node<T> x = this.first; x != null; x = next) {
            next = x.next;
            x.data = null;
            x.next = null;
            x.prev = null;
        }
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    /**
     * The custom toString() method.
     *
     * @return string
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Node<T> x = this.first; x != null; x = x.next) {
            builder.append(x.data.toString());
            builder.append("\n");
        }
        return builder.toString();
    }

    /**
     * Transformation a container into array.
     *
     * @return array of elements
     */
    public Object[] toArray() {
        Object[] result = new Object[this.size];
        int i = 0;
        for (Node<T> x = this.first; x != null; x = x.next) {
            result[i++] = x.data;
        }
        return result;
    }

    /**
     * Creating iterator
     *
     * @return iterator
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int currentPos = 0;

            @Override
            public boolean hasNext() {
                return currentPos < size;
            }

            @Override
            public T next() {
                if (this.hasNext()) {
                    return getByIndex(currentPos++);
                } else {
                    throw new NoSuchElementException();
                }
            }
        };
    }

    /**
     * Добавление данных массива.
     *
     * @param array - входной массив
     */
    public void addAll(final T[] array) {
        for (T i : array) {
            if (i != null) {
                this.pushBack(i);
            }
        }
    }

    public void sort(Comparator<T> comp){
        if (this.getSize() == 0) {
            System.out.println("Nothing to sort!");
        } else {
            if (this.getFirst() == null) {
                System.out.println("Error!");
            } else {
                Node<T> current = this.getFirst();
                Node<T> index = null;
                T temp;
                while(current != null) {
                    index = current.next;
                    while(index != null) {
                        if(comp.compare(current.data, index.data) > 0) {
                            temp = current.data;
                            current.data = index.data;
                            index.data = temp;
                        }
                        index = index.next;
                    }
                    current = current.next;
                }
            }
        }
    }
}