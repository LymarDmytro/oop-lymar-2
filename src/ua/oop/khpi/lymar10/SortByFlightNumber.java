package ua.oop.khpi.lymar10;

import ua.oop.khpi.lymar07.BusStation;
import java.util.Comparator;

public class SortByFlightNumber extends SortFilter<BusStation> {
    public SortByFlightNumber(Comparator<BusStation> comp){
        super(comp);
    }

    @Override
    public int compare(BusStation o1, BusStation o2) {
        float result = o1.getFlightNumber() - o2.getFlightNumber();
        if (super.filter != null) {
            return filter.compare(o1, o2);
        }
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
