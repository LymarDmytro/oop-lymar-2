package ua.oop.khpi.lymar10;

import ua.oop.khpi.lymar07.BusStation;
import java.util.Comparator;

public class SortByDepartureTime  extends SortFilter<BusStation> {
    public SortByDepartureTime(Comparator<BusStation> comp) {
        super(comp);
    }
    @Override
    public int compare(BusStation o1, BusStation o2) {
        String[] ymd1 = o1.getDepartureTime().split(":");
        String[] ymd2 = o2.getDepartureTime().split(":");
        int result = ymd1[1].compareTo(ymd2[1]);
        if (result == 0) {
            result = ymd1[0].compareTo(ymd2[0]);
            if (result == 0) {
                result = ymd1[0].compareTo(ymd2[0]);
            }
        } else if (super.filter != null) {
            return filter.compare(o1, o2);
        }
        return result;
    }
}
