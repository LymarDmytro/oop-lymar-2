package ua.oop.khpi.lymar10;

import ua.oop.khpi.lymar07.BusStation;
import ua.oop.khpi.lymar09.ContainerList;
import ua.oop.khpi.lymar12.StationSearch;

import java.io.*;
import java.util.Scanner;

public final class UI {

    private static boolean isAuto;
    /**
     * Для выбора пунктов в меню.
     */
    private static String choice;
    /**
     * Для ввода.
     */
    private static BufferedReader buffer = new BufferedReader(
            new InputStreamReader(System.in));
    /**
     * Универсальный список.
     */
    public static ContainerList<BusStation> list = new ContainerList<>();
    /**
     * Главный метод диалога.
     * @return true - если выбран выход
     * @throws IOException при ошибках со вводом
     */
    public static boolean run(boolean Auto) throws IOException, ClassNotFoundException {
        isAuto = Auto;
        if(isAuto) {
            autoProcessing();
            return true;
        } else {
            mainMenu();
            boolean flag = mainProcessing();
            System.out.println();
            return flag;
        }
    }
    /**
     * Главное меню диалога.
     * @throws IOException при ошибках со вводом
     */
    private static void mainMenu() throws IOException {
        System.out.println("1. Добавить запись в расписание.");
        System.out.println("2. Удалить запись.");
        System.out.println("3. Очистить список записей.");
        System.out.println("4. Вывод информации.");
        System.out.println("5. Сортировка.");
        System.out.println("6. Сохранить в файл.");
        System.out.println("7. Загрузить из файла.");
        System.out.println("8. Поиск записей.");
        System.out.println("0. Выход.");
        System.out.print("Введите ваш ответ сюда: ");
        choice = buffer.readLine();
        System.out.println();
    }
    /**
     * Обработка выбора главного меню.
     * @return true - если выход
     * @throws IOException при ошибках со вводом
     * @throws ClassNotFoundException при ошибке с классами
    */
    private static boolean mainProcessing() throws IOException, ClassNotFoundException {
        switch (choice) {
            case "1":
                onAdd();
                addProcessing();
                return false;

            case "2":
                onDelete();
                deleteProcessing();
                return false;

            case "3":
                if(list.getSize() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.println("Очистка...");
                list.clear();
                return false;

            case "4":
                if(list.getSize() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.println("Данные: ");
                System.out.print(list.toString());
                return false;

            case "5":
                if(list.getSize() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                onSort();
                sortProcessing();
                return false;

            case "6":
                System.out.println("Сереализация...");
                ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream("Vadim.dat"));
                oos.writeObject(list);
                oos.close();
                System.out.println("Done!\n");
                return false;

            case "7":
                System.out.println("Десериализация  ...");
                ObjectInputStream ois = new ObjectInputStream(
                        new FileInputStream("Vadim.dat"));
                ContainerList<BusStation> secondList =
                        (ContainerList) ois.readObject();
                ois.close();
                System.out.println("Прочитанные данные: ");
                System.out.println(secondList.toString());
                return false;
            case "8":
                if(list.getSize() == 0) {
                    System.out.println("Список пуст!");
                    return false;
                }
                System.out.print(StationSearch.searchEntries(list));
                return false;
            case "0":
                System.out.print("Спасибо за работу!!!");
                return true;
            default:
                return false;
        }
    }
    /**
     * Меню добавления мероприятия.
     * @throws IOException при ошибках со вводом
     */
    private static void onAdd() throws IOException {
        System.out.println("1. Добавить в начало списка.");
        System.out.println("2. Добавить в конец списка.");
        System.out.println("3. Добавить по индексу.");
        System.out.println("4. Прочитать из файла");
        System.out.println("Любая клавиша. Назад.");
        System.out.print("Введите ваш ответ сюда: ");
        choice = buffer.readLine();
        System.out.println();
    }

    /**
     * Обработка выбора меню добавления.
     * @throws IOException при ошибках со вводом
     */
    private static void addProcessing() throws IOException {
        Scanner scan = new Scanner(System.in);
        switch (choice) {
            case "1":
                list.pushFront(BusStation.generateEntry());
                break;

            case "2":
                list.pushBack(BusStation.generateEntry());
                break;

            case "3":
                System.out.print("Введите индекс: ");
                list.insert(scan.nextInt(), BusStation.generateEntry());
                break;
            case "4":
                list.addAll(BusStation.readFromFile("data.txt"));
        }
    }
    /**
     * Меню удаления мероприятия.
     * @throws IOException при ошибках со вводом
     */
    private static void onDelete() throws IOException {
        if (list.getSize() == 0) {
            System.out.println("Список пуст!");
        } else {
            System.out.println("1. Удалить последний.");
            System.out.println("2. Удалить по индексу.");
            System.out.println("Любая клавиша. Назад.");
            System.out.print("Введите ваш ответ сюда: ");
            choice = buffer.readLine();
            System.out.println();
        }
    }
    /**
     * Обработка выбора меню удаления.
     * @throws IOException при ошибках со вводом
     */
    private static void deleteProcessing() throws IOException {
        Scanner scan = new Scanner(System.in);
        switch (choice) {

            case "1" :
                list.popBack();
                break;

            case "2" :
                System.out.print("Введите индекс:");
                list.remove(scan.nextInt());
                break;
        }
    }
    /**
     * Меню сортировки мероприятий.
     * @throws IOException при ошибках со вводом
     */
    private static void onSort() throws IOException {
        if (list.getSize() == 0) {
            System.out.println("Список пуст!");
        } else {
            System.out.println("1. Сортировка по номеру рейса.");
            System.out.println("2. Сортировка по времени отправления.");
            System.out.println("3. Сортировка по количеству свободных мест.");
            System.out.println("Любая клавиша. Назад.");
            System.out.print("Введите ваш ответ сюда: ");
            choice = buffer.readLine();
            System.out.println();
        }
    }
    /** Обработка выбора меню сортировки. */
    private static void sortProcessing() {
        switch (choice) {
            case "1":
                list.sort(new SortByFlightNumber(null));
                break;

            case "2":
                list.sort(new SortByDepartureTime(null));
                break;

            case "3":
                list.sort(new SortByNumberOfFreeSeats(null));
                break;
        }
    }
    /** Програма без диалога. */
    private static void autoProcessing() throws IOException {
        System.out.println("\nДобавим записи:");
        list.addAll(BusStation.readFromFile("data.txt"));
        list.pushBack(BusStation.generateEntry(false));
        System.out.println(list.toString());
        System.out.println("Удалим последнюю запись:");
        list.popBack();
        System.out.print(list.toString());
        System.out.println();
        System.out.println("Отсортируем по номеру рейса: ");
        list.sort(new SortByFlightNumber(null));
        System.out.print(list.toString());
        System.out.println();
        System.out.println("Отсортируем по кол-во свободных мест: ");
        list.sort(new SortByNumberOfFreeSeats(null));
        System.out.print(list.toString());
    }
}
